const protocol = require('http'); 

const port = 4000; 

protocol.createServer((req, res) => { 
   console.log(typeof req); 
   if (req.url === '/') {
  	  console.log(req.url); 
      res.write('Welcome to the Home Page');
      res.end();
   } else if (req.url === '/register') {
   	  console.log(req.url);
   	  res.write('Create Account Here');
   	  res.end(); 
   } else if (req.url === '/products') {
   	  console.log(req.url); 
   	  res.write('List of Products');
   	  res.end(); 
   } else {
   	  res.write('Path Not Recognized');
   	  res.end(); 
   }
   
}).listen(port)

console.log(`Server is Running on port ${port}`);
